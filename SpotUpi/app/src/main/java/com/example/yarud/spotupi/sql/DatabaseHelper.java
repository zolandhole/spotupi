package com.example.yarud.spotupi.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.yarud.spotupi.modal.User;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Spot_upi";
    private static final String TABLE_USER = "user";

    private static final String COLUMN_ID = "id";
    private static final String COLUMN_USERNAME = "username";
    private static final String COLUMN_PASSWORD = "password";

    private String CREATE_USER_TABLE = "CREATE TABLE " +TABLE_USER+ "("
            +COLUMN_ID+ " INTEGER PRIMARY KEY AUTOINCREMENT,"
            +COLUMN_USERNAME+ "TEXT,"
            +COLUMN_PASSWORD+ "TEXT)";

    private String DROP_USER_TABLE = "DROP TABLE IF EXISTS " +TABLE_USER;

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_USER_TABLE);
        onCreate(db);
    }

    public void addUser(User user){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_USERNAME, user.getUsername());
        values.put(COLUMN_PASSWORD, user.getPassword());

        //db.insert(TABLE_USER, null, values);
        //db.close();
        long todo_id = db.insert(TABLE_USER, null, values);
    }

    public void updateUser(User user){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(COLUMN_USERNAME, user.getUsername());
        values.put(COLUMN_PASSWORD, user.getPassword());

        //db.update(TABLE_USER, values, COLUMN_ID + " = ?", new String[]{String.valueOf(user.getId())});
    }

    public boolean checkUser(String username){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(TABLE_USER,
                new String[]{COLUMN_ID, COLUMN_USERNAME, COLUMN_PASSWORD},
                COLUMN_USERNAME + "=?",
                new String[]{username},
                null,null,null);
        if (cursor != null && cursor.moveToFirst() && cursor.getCount()>0){
            return true;
        }
        return false;
    }
}
