package com.example.yarud.spotupi.modal;


public class User {
    private int iduser;
    private String username;
    private String password;
    private String kodedosen;
    public User()
    {
    }
    public User(int iddosen, String username, String password, String kodedosen)
    {
        this.iduser=iddosen;
        this.username=username;
        this.password=password;
        this.kodedosen=kodedosen;

    }

    public User(User user){
        this.iduser = user.getIduser();
    }
    public User(int iduser){
        this.iduser=iduser;
    }

    public void setIduser(int iduser){this.iduser = iduser;}
    public void setUsername(String username) {
        this.username = username;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setKodedosen(String kodedosen) {
        this.kodedosen = kodedosen;
    }

    public int getIduser() {return iduser;}
    public String getUsername() {
        return username;
    }
    public String getPassword() {
        return password;
    }
    public String getKodedosen() {
        return kodedosen;
    }
}
