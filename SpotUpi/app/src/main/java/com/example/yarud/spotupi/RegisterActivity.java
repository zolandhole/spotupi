package com.example.yarud.spotupi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.yarud.spotupi.modal.User;
import com.example.yarud.spotupi.sql.DBHandler;
import com.example.yarud.spotupi.sql.DatabaseHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {
    private final AppCompatActivity activity = RegisterActivity.this;
    private DBHandler dbHandler;
    private User user;

    EditText editTextUsername, editTextPassword;
    Button buttonLogin;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        editTextUsername = findViewById(R.id.EditTextUsername);
        editTextPassword = findViewById(R.id.EditTextPassword);
        progressDialog = new ProgressDialog(this);
        buttonLogin = findViewById(R.id.ButtonLogin);

        dbHandler = new DBHandler(activity);
        user = new User();

        final String username = editTextUsername.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        //final String username = editTextUsername.getText().toString().trim();
        //final String password = editTextPassword.getText().toString().trim();
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(editTextUsername.getText())){
                    editTextUsername.setError("NIP harus dimasukan");
                } else if (TextUtils.isEmpty(editTextPassword.getText())){
                    editTextPassword.setError("Password perlu dimasukan");
                }else{
                    Log.w("USERNAME", username);
                    //getToken(username, password);
                }
            }
        });
    }

    public void getToken(final String username, final String password){
        progressDialog.setMessage("Mohon menunggu ...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Url.GET_TOKEN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String getToken = jsonObject.getString("token");
                            getUser(getToken, username, password);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();
                        Toast.makeText(getApplicationContext(),"Silahkan periksa kembali username password anda",Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() {
                Map<String,String> params = new HashMap<>();
                params.put("username", username);
                params.put("password", password);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void postDataToSQLite(String username, String password){
        user.setUsername(username);
        user.setPassword(password);
        if (dbHandler.checkUser(username)){
            dbHandler.updateUser(user);
        } else {
            dbHandler.addUser(user);
        }
    }

    public void getUser(final String getToken, final String username, final String password){
        StringRequest stringRequestUser = new StringRequest(Request.Method.GET,
                Url.GET_USER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //progressDialog.dismiss();
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray nama = jsonObject.getJSONArray("dt_user");
                            String name = "";
                            String kode = "";
                            for(int i=0;i<nama.length();i++){
                                JSONObject gelar = nama.getJSONObject(i);
                                name = gelar.getString("GELARNAMA");
                                kode = gelar.getString("KODEDOSEN");
                            }

                            postDataToSQLite(username, password);
                            Intent main_activity_intent = new Intent(activity, MainActivity.class);
                            main_activity_intent.putExtra("GELARNAMA", name);
                            main_activity_intent.putExtra("KODEDOSEN", kode);
                            startActivity(main_activity_intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("TAG", "Error = "+ error);
                    }
                }){
            @Override
            public Map getHeaders() {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer "+getToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequestUser);
    }
}
