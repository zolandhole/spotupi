package com.example.yarud.spotupi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.app.ProgressDialog;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.yarud.spotupi.modal.User;
import com.example.yarud.spotupi.sql.DBHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    private final AppCompatActivity activityMain = MainActivity.this;
    private TextView txtUsername;
    private DBHandler dbHandler;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtUsername = findViewById(R.id.txtUsername);
        progressDialog = new ProgressDialog(this);

        dbHandler = new DBHandler(activityMain);
        if (dbHandler.checkDB()){
            Intent intentLogin = new Intent(this,RegisterActivity.class);
            startActivity(intentLogin);
        } //else {
        //    getTokenFromDB();
        //}
        //Intent intent = getIntent();
        //String gelarnama = intent.getStringExtra("GELARNAMA");
        //String kodedosen = intent.getStringExtra("KODEDOSEN");

        //txtUsername.setText(gelarnama+" - "+kodedosen);
    }

    public void getTokenFromDB(){
        int idUser = dbHandler.getLastId();
        User user = dbHandler.getUser(idUser);
        String nip = user.getUsername();
        String pass = user.getPassword();
        getToken(nip, pass);
    }
    public void getToken(final String username, final String password){
        progressDialog.setMessage("Mohon menunggu ...");
        progressDialog.show();
        progressDialog.setCancelable(false);
        StringRequest stringRequest = new StringRequest(Request.Method.POST,
                Url.GET_TOKEN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            String dataToken = jsonObject.getString("token");
                            verifyToken(dataToken, username, password);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.hide();
                        Toast.makeText(getApplicationContext(),"Silahkan periksa kembali username password anda",Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() {
                Map<String,String> params = new HashMap<>();
                params.put("username", username);
                params.put("password", password);
                return params;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void verifyToken(String dataToken, String username, String password){
        String search  = "eyJ0e";
        if (dataToken.toLowerCase().contains(search.toLowerCase())){
            getUser(dataToken,username,password);
        }else{
            Intent intentRegister = new Intent(activityMain, RegisterActivity.class);
            startActivity(intentRegister);
        }
    }

    public void getUser(final String dataToken, final String username, final String password){
        StringRequest stringRequestUser = new StringRequest(Request.Method.GET,
                Url.GET_USER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            JSONArray nama = jsonObject.getJSONArray("dt_user");
                            String gelarnama = "";
                            String kodedosen = "";
                            for(int i=0;i<nama.length();i++){
                                JSONObject gelar = nama.getJSONObject(i);
                                gelarnama = gelar.getString("GELARNAMA");
                                kodedosen = gelar.getString("KODEDOSEN");
                            }

                            postDataToSQLite(username, password);
                            txtUsername.setText(gelarnama+" - "+kodedosen);
                            progressDialog.dismiss();
                            txtUsername.setVisibility(View.VISIBLE);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("TAG", "Error = "+ error);
                    }
                }){
            @Override
            public Map getHeaders() {
                HashMap headers = new HashMap();
                headers.put("Content-Type", "application/json");
                headers.put("Authorization", "Bearer "+dataToken);
                return headers;
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequestUser);
    }

    private void postDataToSQLite(String username, String password){
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        if (dbHandler.checkUser(username)){
            dbHandler.updateUser(user);
        } else {
            dbHandler.addUser(user);
        }
    }
}
