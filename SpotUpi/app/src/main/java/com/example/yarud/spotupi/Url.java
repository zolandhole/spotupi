package com.example.yarud.spotupi;

public class Url {

    //untuk mengambil token dari JWT UPI
    public static final String GET_TOKEN = "http://api.upi.edu/jwt/api/user/login";
    public static final String GET_USER = "http://api.upi.edu/jwt/api/user";

}
